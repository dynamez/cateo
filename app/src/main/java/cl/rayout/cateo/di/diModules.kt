package cl.rayout.cateo.di

import cl.rayout.cateo.repositories.LoginRepository
import cl.rayout.cateo.repositories.ReservationDashboardRepository
import cl.rayout.cateo.viewmodels.*
import cl.rayout.cateo.wrappers.FirebaseWrapper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModules = module {
    single { LoginRepository(get()) }
    single { ReservationDashboardRepository(get()) }
    single { FirebaseWrapper() }
    viewModel { RecoverPasswordViewModel(get()) }
    viewModel { ToolDetailsViewModel(get()) }
    viewModel { ToolListViewModel(get()) }
    viewModel { MainDashboardViewModel(get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { RegistrationViewModel(get()) }
}