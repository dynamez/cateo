package cl.rayout.cateo

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.nav_header_main.*
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        //Crashlytics.getInstance().crash()
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_container)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.mainDashboard,
                R.id.toolList
            ), drawerLayout
        )



        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

            }

            override fun onDrawerClosed(drawerView: View) {

            }

            override fun onDrawerOpened(drawerView: View) {
                Timber.d("open")
                currentUser.text = FirebaseAuth.getInstance().currentUser?.email
            }
        })
        navView.setNavigationItemSelectedListener {
            return@setNavigationItemSelectedListener when (it.itemId) {
                R.id.mainDashboard -> {
                    Timber.d("Maindashboard selected")
                    navController.navigate(R.id.mainDashboard)
                    drawerLayout.closeDrawers()
                    true
                }
                R.id.toolList -> {
                    Timber.d("Toollist selected")
                    navController.navigate(R.id.toolList)
                    drawerLayout.closeDrawers()
                    true
                }
                R.id.logoutSession -> {
                    Timber.d("logout")
                    navController.navigate(R.id.action_mainDashboard_to_mainMenu2)
                    FirebaseAuth.getInstance().signOut()
                    drawerLayout.closeDrawers()
                    true
                }
                else -> {
                    Timber.d("else")
                    super.onOptionsItemSelected(it)
                }
            }
        }

    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//
//        return when (item.itemId) {
//            R.id.mainDashboard -> {
//                Timber.d("Custom selected ${item.itemId}")
//                true
//            }
////            android.R.id.home -> {
////                onBackPressed()
////                true
////            }
//            else -> {
//                Timber.d("Custom selected ${item.itemId}")
//                super.onOptionsItemSelected(item)
//            }
//        }
//
//
//    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_container)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
