package cl.rayout.cateo.common.recycler

interface AdapterListener {
    fun listen(click: AdapterClick?)
}