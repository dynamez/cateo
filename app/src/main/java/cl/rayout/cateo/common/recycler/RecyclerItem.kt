package cl.rayout.cateo.common.recycler

interface RecyclerItem {
    val id: String?
    override fun equals(other: Any?): Boolean
}