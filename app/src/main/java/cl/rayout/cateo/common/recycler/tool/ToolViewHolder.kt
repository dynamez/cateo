package cl.rayout.cateo.common.recycler.tool

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import cl.rayout.cateo.models.Tool
import kotlinx.android.synthetic.main.tool_item.view.*
import kotlinx.android.synthetic.main.tool_reservation_item.view.deliveryDate
import kotlinx.android.synthetic.main.tool_reservation_item.view.toolName
import java.text.SimpleDateFormat

class ToolViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(tool: Tool) {
        itemView.toolName.text = tool.name
        itemView.deliveryDate.text =
            SimpleDateFormat("dd-MM-yyyy HH:mm").format(tool.deliveryDate?.toDate()).toString()
        if (tool.available != null)
            itemView.toolStatus.text = when (tool.available) {
                true -> "Disponible"
                false -> "No disponible"
                else -> "Disponible"
            }

    }

}