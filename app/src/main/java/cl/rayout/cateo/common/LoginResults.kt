package cl.rayout.cateo.common

enum class LoginResults(status: String) {
    LOGIN_FAILED("LOGIN_FAILED"),
    LOGIN_SUCCESS("LOGIN_SUCCESS")
}

sealed class LoginRegistrationResults {
    object LoginSuccess : LoginRegistrationResults()
    object RegistrationSuccess : LoginRegistrationResults()
    class LoginError(val errorCode: String?) : LoginRegistrationResults()
    class RegistrationError(val errorCode: String?) : LoginRegistrationResults()
    object RecoverSuccessful : LoginRegistrationResults()
    class RecoverError(val errorCode: String?) : LoginRegistrationResults()

}