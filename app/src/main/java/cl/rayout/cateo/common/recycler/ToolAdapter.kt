package cl.rayout.cateo.common.recycler

import cl.rayout.cateo.common.recycler.tool.ToolCell

class ToolAdapter(listener: AdapterListener) : BaseListAdapter(
    ToolCell,
    listener = listener
)