package cl.rayout.cateo.common.recycler.tool

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.rayout.cateo.R
import cl.rayout.cateo.common.recycler.AdapterListener
import cl.rayout.cateo.common.recycler.Cell
import cl.rayout.cateo.common.recycler.RecyclerItem
import cl.rayout.cateo.models.Tool

object ToolCell : Cell<RecyclerItem>() {

    override fun belongsTo(item: RecyclerItem?): Boolean {
        return item is Tool
    }

    override fun type(): Int {
        return R.layout.tool_item
    }

    override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ToolViewHolder(parent.viewOf(type()))
    }

    override fun bind(
        holder: RecyclerView.ViewHolder,
        item: RecyclerItem?,
        listener: AdapterListener?
    ) {
        if (holder is ToolViewHolder && item is Tool) {
            holder.bind(item)
            holder.itemView.setOnClickListener {
                listener?.listen(item)
            }
        }
    }

}