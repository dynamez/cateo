package cl.rayout.cateo.models

import android.os.Parcelable
import cl.rayout.cateo.common.recycler.AdapterClick
import cl.rayout.cateo.common.recycler.RecyclerItem
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tool(
    override val id: String?,
    val name: String? = "",
    val description: String? = "",
    var startDate: Timestamp? = Timestamp.now(),
    var deliveryDate: Timestamp? = Timestamp.now(),
    var available: Boolean? = true,
    val lastMaintenance: Timestamp? = Timestamp.now(),
    val lastReservation: Timestamp? = Timestamp.now(),
    val lastUser: String? = "",
    val casillero: Long?,
    val openchances: Long? = 2
) : RecyclerItem, AdapterClick, Parcelable