package cl.rayout.cateo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.extensions.hideActionBar
import cl.rayout.cateo.viewmodels.MainLoginViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.main_login_fragment.*

class MainMenu : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var viewModel: MainLoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideActionBar()
        auth = FirebaseAuth.getInstance()
        viewModel = ViewModelProvider(this).get(MainLoginViewModel::class.java)
        // TODO: Use the ViewModel
        registerBtn.setOnClickListener {
            findNavController().navigate(R.id.action_mainMenu2_to_registration)
        }
        loginBtn.setOnClickListener {
            findNavController().navigate(R.id.action_mainMenu2_to_login)
        }
    }

    override fun onResume() {
        super.onResume()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {
            findNavController().navigate(R.id.action_global_mainMenu2)
        }
    }
}
