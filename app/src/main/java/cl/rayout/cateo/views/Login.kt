package cl.rayout.cateo.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import cl.rayout.cateo.R
import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.extensions.alertDialog
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.extensions.hideActionBar
import cl.rayout.cateo.viewmodels.LoginViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.login_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class Login : Fragment() {

    private lateinit var auth: FirebaseAuth
    private val viewModel: LoginViewModel by viewModel()
    private var sessionLoader: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideActionBar()
        val callback = object : OnBackPressedCallback(true /* enabled by default */) {
            override fun handleOnBackPressed() {
                Timber.d("nav up called")
                findNavController().navigate(R.id.action_login_to_mainMenu2)
            }
        }
        activity?.onBackPressedDispatcher?.addCallback(this, callback)
        auth = FirebaseAuth.getInstance()

        recoverBtn.setOnClickListener {
            findNavController().navigate(R.id.action_login_to_recoverPassword)
        }
        loginBtn.setOnClickListener {
            if (email.text.isNullOrEmpty() || password.text.isNullOrEmpty()) {
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Todos los campos son obligatorios")
                    setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                    }
                }
            } else {
                sessionLoader = alertDialog {
                    setMessage("Cargando...")
                }
                viewModel.processLogin(
                    user = email.text.toString(),
                    pass = password.text.toString()
                )
            }

        }

        viewModel.loginResultLiveData.observe(
            viewLifecycleOwner,
            Observer { result: LoginRegistrationResults ->
                sessionLoader?.dismiss()
                when (result) {
                    is LoginRegistrationResults.LoginSuccess -> {
                        Timber.d("SUCCESS")
                        findNavController().navigate(R.id.action_login_to_mainDashboard)
                    }
                    is LoginRegistrationResults.LoginError -> {
                        Timber.d(result.errorCode)
                        Toast.makeText(
                            requireContext(),
                            "Credenciales incorrectas",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })
        // TODO: Use the ViewModel
    }

    override fun onResume() {
        super.onResume()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {
            findNavController().navigate(R.id.action_login_to_mainDashboard)
        }
    }
}
