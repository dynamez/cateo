package cl.rayout.cateo.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import cl.rayout.cateo.R
import cl.rayout.cateo.extensions.alertDialog
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.viewmodels.RecoverPasswordViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.recover_password_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RecoverPassword : Fragment() {

    private val viewModel: RecoverPasswordViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recover_password_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recoverBtn.setOnClickListener {
            if (recoverEmail.text.isNullOrEmpty()) {
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Todos los campos son obligatorios")
                    setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                    }
                }
            } else {
                viewModel.passwordRecoveryProcess(recoverEmail.text.toString())
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Si tu correo está registrado recibirás un email con los pasos para recuperar tu cuenta.")
                    setPositiveButton("OK"){dialog, which ->
                        dialog.dismiss()
                        findNavController().navigate(R.id.action_recoverPassword_to_mainMenu2)
                    }
                }
            }
        }
    }

}
