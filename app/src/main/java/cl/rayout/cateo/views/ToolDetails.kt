package cl.rayout.cateo.views


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import cl.rayout.cateo.R
import cl.rayout.cateo.databinding.ToolDetailsFragmentBinding
import cl.rayout.cateo.extensions.alertDialog
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.extensions.hideActionBar
import cl.rayout.cateo.extensions.toast
import cl.rayout.cateo.viewmodels.ToolDetailsViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.tool_details_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.slybeaver.slycalendarview.SlyCalendarDialog
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*


class ToolDetails : Fragment(), SlyCalendarDialog.Callback {

    private lateinit var dataBinder: ToolDetailsFragmentBinding
    private val viewModel: ToolDetailsViewModel by viewModel()
    private val args: ToolDetailsArgs by navArgs()
    var currentUser: FirebaseUser? = null
    private var loadingDialog: AlertDialog? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.tool_details_fragment, container, false)
        viewModel.start(args.tool, args.available)
        dataBinder = ToolDetailsFragmentBinding.bind(view).apply {
            model = viewModel
        }
        dataBinder.lifecycleOwner = this.viewLifecycleOwner
        return dataBinder.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideActionBar()
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        viewModel.openLockerResponse.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it) {
                loadingDialog?.dismiss()
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Casillero abierto. Se cerrará automáticamente en 30 segundos")
                    setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                    }
                }
            }
        })
        viewModel.canOpenLocker.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it) {
                loadingDialog = alertDialog {
                    setTitle("Cateo")
                    setMessage("Abriendo casillero...")
                }
                viewModel.openLocker(currentUser?.uid, args.tool?.id)
            } else {
                alertDialog {
                    setTitle("Cateo")
                    setMessage("No fue posible abrir el casillero")
                    setPositiveButton("Ok") { dialog, which ->
                        dialog.dismiss()
                    }
                }
            }
        })
        openLockerBtn.setOnClickListener {
            alertDialog {
                setTitle("Advertencia")
                setMessage("Recuerda que solo podrás abrir el casillero 2 veces, para retirar y entregar. Deseas continuar?")
                setPositiveButton("Continuar") { dialog, which ->
                    viewModel.checkIfCanOpenLocker(currentUser?.uid, args.tool?.id)
                }
                setNegativeButton("Cancelar") { dialog, which ->
                    dialog.dismiss()
                }

            }
        }
        viewModel.lockerDeliver.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it) {
                loadingDialog?.dismiss()
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Casillero abierto. Deja la herramienta en su lugar correspondiente.")
                    setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                        viewModel.deliverToolbyUser(
                            currentUser = currentUser?.uid!!,
                            email = currentUser?.email ?: ""
                        )
                        loadingDialog = alertDialog {
                            setMessage("Cargando...")
                        }
                    }
                }

            }
        })
        viewModel.reservaytionLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loadingDialog?.dismiss()
            if (it) {
                toast("Herramienta reservada")
                findNavController().navigate(R.id.action_toolDetails_to_mainDashboard)
            } else {
                toast("No fue posible reservar")
                findNavController().navigate(R.id.action_toolDetails_to_mainDashboard)
            }
        })
        viewModel.deliverLiveData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            loadingDialog?.dismiss()
            if (it) {
                toast("Herramienta entregada")
            } else {
                toast("Error entregando herramienta")
            }
            findNavController().navigate(R.id.action_toolDetails_to_mainDashboard)
        })
        reserveBtn.setOnClickListener {

            if (!args.available) {


                if (currentUser != null) {
                    alertDialog {
                        setTitle("Advertencia")
                        setMessage("Recuerda que al abrir el casillero tendrás solo 30 segundos para entregar. Deseas continuar?")
                        setPositiveButton("Continuar") { dialog, which ->
                            loadingDialog = alertDialog {
                                setMessage("Cargando...")
                            }
                            viewModel.deliverOpenLocker(currentUser?.uid, args.tool?.id)
                        }
                        setNegativeButton("Cancelar") { dialog, which ->
                            dialog.dismiss()
                        }

                    }


                }

            } else {
                val c = Calendar.getInstance()
                val calendarDialog = SlyCalendarDialog()
                    .setCallback(this)
                    .setSingle(false)
                    .show(parentFragmentManager, "")
//                val datePickerDialog = DatePickerDialog.newInstance(this,c[Calendar.YEAR],c[Calendar.MONTH],c[Calendar.DAY_OF_MONTH])
//                datePickerDialog.show(parentFragmentManager, "")

//                val dpd = DatePickerDialog(
//                    requireContext(),
//                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
//                        loadingDialog = alertDialog {
//                            setMessage("Cargando...")
//                        }
//                        // Display Selected date in textbox
//                        Timber.d("año $year, mes $monthOfYear, dia $dayOfMonth")
//                        val c = Calendar.getInstance()
//                        c[Calendar.YEAR] = year
//                        c[Calendar.MONTH] = monthOfYear
//                        c[Calendar.DAY_OF_MONTH] = dayOfMonth
//                        c.time
//                        val date = Date(year, monthOfYear, dayOfMonth)
//
//                        var currentDate = Date()
//                        val formatter = SimpleDateFormat("dd-MM-yyyy HH:mma")
//                        val answer: String = formatter.format(currentDate)
//                        val pickedDate = formatter.format(c.time)
//                        if (c.time < currentDate){
//                            loadingDialog?.dismiss()
//                            alertDialog {
//                                setTitle("Cateo")
//                                setMessage("No puedes elegir una fecha anterior al día de hoy")
//                                setPositiveButton("OK"){dialog, _ ->
//                                    dialog.dismiss()
//                                }
//                            }
//                        }else{
//                            if (currentUser != null) {
//                                viewModel.reserveToolByUser(
//                                    currentUser = currentUser?.email ?: "",
//                                    date = c.time,
//                                    userId = currentUser?.uid ?: ""
//                                )
//
//                            }
//                        }
//                        Timber.d("picked date is ${pickedDate}")
//                        Timber.d("Current date is $answer")
//
////                    lblDate.setText("" + dayOfMonth + " " + MONTHS[monthOfYear] + ", " + year)
//                    },
//                    year,
//                    month,
//                    day
//                )
//
//                dpd.show()


            }
            // findNavController().navigate(R.id.action_toolDetails_to_mainDashboard)
        }
    }

    override fun onDataSelected(
        firstDate: Calendar?,
        secondDate: Calendar?,
        hours: Int,
        minutes: Int
    ) {
        Timber.d("Primera fecha es ${firstDate?.time}, Segunda fecha es ${secondDate?.time}")
        loadingDialog = alertDialog {
            setMessage("Cargando...")
        }
        var currentDate = Date()
        val formatter = SimpleDateFormat("dd-MM-yyyy HH:mma")
        val answer: String = formatter.format(currentDate)
        val pickedDate = formatter.format(firstDate?.time)
        if (firstDate?.time!! < currentDate) {
            loadingDialog?.dismiss()
            alertDialog {
                setTitle("Cateo")
                setMessage("No puedes elegir una fecha anterior al día de hoy")
                setPositiveButton("OK") { dialog, _ ->
                    dialog.dismiss()
                }
            }
        } else {
            if (currentUser != null) {
                viewModel.reserveToolByUser(
                    currentUser = currentUser?.email ?: "",
                    date = firstDate.time,
                    endDate = secondDate?.time,
                    userId = currentUser?.uid ?: ""
                )

            }
        }
    }

    override fun onCancelled() {

    }

    override fun onResume() {
        super.onResume()
        currentUser = FirebaseAuth.getInstance().currentUser
    }

}
