package cl.rayout.cateo.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cl.rayout.cateo.R
import cl.rayout.cateo.common.recycler.AdapterClick
import cl.rayout.cateo.common.recycler.AdapterListener
import cl.rayout.cateo.common.recycler.ToolAdapter
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.extensions.gone
import cl.rayout.cateo.extensions.show
import cl.rayout.cateo.extensions.showActionBar
import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.viewmodels.MainDashboardViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.main_dashboard_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainDashboard : Fragment(), AdapterListener {

    private val viewModel: MainDashboardViewModel by viewModel()
    private val listAdapter by lazy { ToolAdapter(this) }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_dashboard_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialize()
        showActionBar()
        (activity as AppCompatActivity).supportActionBar?.title = "Tus Reservas"
        viewModel.mutableToolList.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                listAdapter.submitList(it)
            } else {
                lottie.show()
                emptyLabel.show()
            }
            progressBar.gone()
            loadingLabel.gone()
        })

        reserveButton.setOnClickListener {
            findNavController().navigate(R.id.action_mainDashboard_to_toolList)
        }
        refreshView.setOnRefreshListener {
            val currentUser = FirebaseAuth.getInstance().currentUser
            if (currentUser != null) {
                viewModel.getReservationData(currentUser = currentUser.uid)
            }
            refreshView.isRefreshing = false
        }

    }

    private fun initialize() {
        recycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = listAdapter
        }
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            viewModel.getReservationData(currentUser = currentUser.uid)
        }
    }

    override fun listen(click: AdapterClick?) {
        when (click) {
            is Tool -> {
                val directions = MainDashboardDirections.actionMainDashboardToToolDetails(
                    click, false
                )
                findNavController().navigate(directions)
                Timber.d("Tool clicked!!")
            }

        }
    }


}
