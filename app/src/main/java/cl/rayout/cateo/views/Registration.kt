package cl.rayout.cateo.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import cl.rayout.cateo.R
import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.extensions.alertDialog
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.viewmodels.RegistrationViewModel
import kotlinx.android.synthetic.main.login_fragment.*
import kotlinx.android.synthetic.main.registration_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class Registration : Fragment() {

    private val viewModel: RegistrationViewModel by viewModel()
    private var sessionLoader: AlertDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.registration_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recoverPwdBtn.setOnClickListener {
            findNavController().navigate(R.id.action_registration_to_recoverPassword)
        }
        registerBtn.setOnClickListener {
            if (registerUser.text.isNullOrEmpty() || registerPassword.text.isNullOrEmpty()) {
                alertDialog {
                    setTitle("Cateo")
                    setMessage("Todos los campos son obligatorios")
                    setPositiveButton("OK") { dialog, which ->
                        dialog.dismiss()
                    }
                }
            } else {
                sessionLoader = alertDialog {
                    setMessage("Cargando...")
                }
                viewModel.registrationProcess(
                    user = registerUser.text.toString(),
                    password = registerPassword.text.toString()
                )
            }
        }
        viewModel.registrationLiveData.observe(viewLifecycleOwner, Observer {
            sessionLoader?.dismiss()
            when (it) {
                is LoginRegistrationResults.RegistrationSuccess -> {
                    Timber.d("Registration success")
                    Toast.makeText(requireContext(), "Registro exitoso", Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.action_registration_to_mainDashboard)
                }
                is LoginRegistrationResults.RegistrationError -> {
                    Toast.makeText(
                        requireContext(),
                        "No fue posible realizar el registro",
                        Toast.LENGTH_SHORT
                    ).show()
                    Timber.d("Registration error ${it.errorCode}")
                }
            }
        })
    }

}
