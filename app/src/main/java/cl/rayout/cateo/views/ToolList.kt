package cl.rayout.cateo.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cl.rayout.cateo.R
import cl.rayout.cateo.common.recycler.AdapterClick
import cl.rayout.cateo.common.recycler.AdapterListener
import cl.rayout.cateo.common.recycler.ToolAdapter
import cl.rayout.cateo.extensions.findNavController
import cl.rayout.cateo.extensions.gone
import cl.rayout.cateo.extensions.show
import cl.rayout.cateo.extensions.showActionBar
import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.viewmodels.ToolListViewModel
import kotlinx.android.synthetic.main.tool_list_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ToolList : Fragment(), AdapterListener {

    private val viewModel: ToolListViewModel by viewModel()
    private val listAdapter by lazy { ToolAdapter(this) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.tool_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialize()
        showActionBar()
        (activity as AppCompatActivity).supportActionBar?.title = "Listado de Herramientas"
        viewModel.mutableToolList.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                listAdapter.submitList(it)

            } else {
                emptyLabelList.show()
                lottieList.show()
            }
            progressBarList.gone()
            loadingLabelList.gone()
        })

        refreshView.setOnRefreshListener {
            viewModel.fetchToolData()
            refreshView.isRefreshing = false
        }

    }

    private fun initialize() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = listAdapter
        }
        viewModel.fetchToolData()
    }

    override fun listen(click: AdapterClick?) {
        when (click) {
            is Tool -> {
                val directions = ToolListDirections.actionToolListToToolDetails(
                    click, true
                )
                findNavController().navigate(directions)
                Timber.d("Tool clicked!!")

            }

        }
    }

}
