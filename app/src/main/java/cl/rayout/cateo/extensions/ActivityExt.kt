package cl.rayout.cateo.extensions

import android.app.Activity
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, message, duration).show()

inline fun Activity.alertDialog(body: AlertDialog.Builder.() -> AlertDialog.Builder): AlertDialog {
    return AlertDialog.Builder(this)
        .body()
        .show()
}

fun Activity.hideActionBar() {
    (this as AppCompatActivity).supportActionBar?.hide()
}

fun Activity.showActionBar() {
    (this as AppCompatActivity).supportActionBar?.show()
}