package cl.rayout.cateo.extensions

import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment

fun Fragment.findNavController(): NavController =
    NavHostFragment.findNavController(this)

fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) =
    activity?.toast(message, duration)

inline fun Fragment.alertDialog(body: AlertDialog.Builder.() -> AlertDialog.Builder) =
    activity?.alertDialog(body)

fun Fragment.hideActionBar() {
    activity?.hideActionBar()
}

fun Fragment.showActionBar() {
    activity?.showActionBar()
}
