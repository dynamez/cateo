package cl.rayout.cateo.repositories

import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.wrappers.FirebaseWrapper

class LoginRepository(private val wrapper: FirebaseWrapper) {

    suspend fun loginProcess(user: String, password: String): LoginRegistrationResults {
        return wrapper.loginProcess(user, password)
    }

    suspend fun registrationProcess(user: String, password: String): LoginRegistrationResults {
        return wrapper.registrationProcess(user, password)
    }

    suspend fun passwordRecoveryProcess(user: String?): LoginRegistrationResults {
        return wrapper.passwordRecoveryProcess(user = user)
    }

}