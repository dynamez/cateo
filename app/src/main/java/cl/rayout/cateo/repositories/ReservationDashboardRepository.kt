package cl.rayout.cateo.repositories

import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.wrappers.FirebaseWrapper
import java.util.*

class ReservationDashboardRepository(private val firebaseWrapper: FirebaseWrapper) {

    suspend fun getUserReservations(currentUser: String): List<Tool> {
        return firebaseWrapper.getUserReservations(currentUser)
    }

    suspend fun fetchToolData(): List<Tool> {
        return firebaseWrapper.fetchToolData()
    }

    suspend fun deliverToolbyUser(currentUser: String, idTool: String, email: String): Boolean {
        return firebaseWrapper.deliverToolbyUser(currentUser, idTool = idTool, email = email)
    }

    suspend fun reserveToolByUser(
        currentUser: String,
        tool: Tool,
        date: Date?,
        userId: String, endDate: Date?

    ): Boolean {
        return firebaseWrapper.reserveToolByUser(
            currentUser = currentUser,
            tool = tool,
            date = date,
            userId = userId,
            endDate = endDate
        )
    }

    suspend fun checkIfCanOpenLocker(user: String?, toolId: String?): Boolean {
        return firebaseWrapper.checkIfCanOpenLocker(user = user, toolId = toolId)
    }

    suspend fun openLocker(user: String?, toolId: String?): Boolean {
        return firebaseWrapper.openLocker(user = user, toolId = toolId)
    }
}