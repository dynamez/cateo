package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.repositories.ReservationDashboardRepository
import kotlinx.coroutines.launch

class ToolListViewModel(private val repository: ReservationDashboardRepository) : ViewModel() {
    val mutableToolList: MutableLiveData<List<Tool>> = MutableLiveData()

    fun fetchToolData() {
        viewModelScope.launch {
            mutableToolList.value = repository.fetchToolData()
        }
    }
}
