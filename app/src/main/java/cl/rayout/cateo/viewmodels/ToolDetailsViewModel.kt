package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.repositories.ReservationDashboardRepository
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class ToolDetailsViewModel(private val repository: ReservationDashboardRepository) : ViewModel() {
    val mutableToolLiveData: MutableLiveData<Tool> = MutableLiveData()
    val livedataStatus: MutableLiveData<Boolean> = MutableLiveData()
    val canOpenLocker: MutableLiveData<Boolean> = MutableLiveData()
    val openLockerResponse: MutableLiveData<Boolean> = MutableLiveData()
    val reservaytionLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val deliverLiveData: MutableLiveData<Boolean> = MutableLiveData()
    val lockerDeliver: MutableLiveData<Boolean> = MutableLiveData()

    fun start(tool: Tool?, reserved: Boolean) {
        mutableToolLiveData.value = tool
        livedataStatus.value = reserved
    }

    fun deliverToolbyUser(currentUser: String, email: String) {
        viewModelScope.launch {
            deliverLiveData.value = repository.deliverToolbyUser(
                currentUser,
                idTool = mutableToolLiveData.value?.id!!,
                email = email
            )
        }
    }

    fun deliverOpenLocker(user: String?, toolId: String?) {
        viewModelScope.launch {
            lockerDeliver.postValue(repository.openLocker(user, toolId))
        }
    }

    fun reserveToolByUser(currentUser: String, date: Date?, endDate: Date?, userId: String) {
        viewModelScope.launch {
            reservaytionLiveData.value = repository.reserveToolByUser(
                currentUser = currentUser,
                tool = mutableToolLiveData.value!!,
                date = date,
                userId = userId,
                endDate = endDate
            )
        }
    }

    fun openLocker(user: String?, toolId: String?) {
        viewModelScope.launch {
            openLockerResponse.postValue(repository.openLocker(user, toolId))
        }
    }

    fun checkIfCanOpenLocker(user: String?, toolId: String?) {
        viewModelScope.launch {
            canOpenLocker.postValue(repository.checkIfCanOpenLocker(user, toolId))
        }
    }

    fun formattedDate(date: Date): String {
        return SimpleDateFormat("dd-MM-yyyy HH:mm").format(date).toString()
    }
}
