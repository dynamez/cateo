package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.models.Tool
import cl.rayout.cateo.repositories.ReservationDashboardRepository
import kotlinx.coroutines.launch

class MainDashboardViewModel(private val repository: ReservationDashboardRepository) : ViewModel() {
    val mutableToolList: MutableLiveData<List<Tool>> = MutableLiveData()

    fun getReservationData(currentUser: String) {
        viewModelScope.launch {
            mutableToolList.value = repository.getUserReservations(currentUser)
        }
    }
}
