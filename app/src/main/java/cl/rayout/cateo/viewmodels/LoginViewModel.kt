package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.repositories.LoginRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: LoginRepository) : ViewModel() {
    val loginResultLiveData: MutableLiveData<LoginRegistrationResults> = MutableLiveData()

    fun processLogin(user: String, pass: String) {
        viewModelScope.launch {
            loginResultLiveData.postValue(repository.loginProcess(user, pass))
        }
    }
}
