package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.repositories.LoginRepository
import kotlinx.coroutines.launch

class RecoverPasswordViewModel(private val repository: LoginRepository) : ViewModel() {
    val recoverResultLiveData: MutableLiveData<LoginRegistrationResults> = MutableLiveData()

    fun passwordRecoveryProcess(user: String?) {
        viewModelScope.launch {
            recoverResultLiveData.postValue(repository.passwordRecoveryProcess(user))
        }
    }
}
