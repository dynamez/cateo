package cl.rayout.cateo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.repositories.LoginRepository
import kotlinx.coroutines.launch

class RegistrationViewModel(private val repository: LoginRepository) : ViewModel() {
    val registrationLiveData: MutableLiveData<LoginRegistrationResults> = MutableLiveData()

    fun registrationProcess(user: String, password: String) {
        viewModelScope.launch {
            registrationLiveData.value = repository.registrationProcess(user, password)
        }
    }

    fun passwordRecoveryProcess(user: String) {
        viewModelScope.launch {
            registrationLiveData.value = repository.passwordRecoveryProcess(user)
        }
    }
}
