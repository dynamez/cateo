package cl.rayout.cateo.wrappers

import cl.rayout.cateo.common.LoginRegistrationResults
import cl.rayout.cateo.models.Tool
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import java.util.*
import kotlin.collections.HashMap

class FirebaseWrapper {
    private val firebaseAuth = FirebaseAuth.getInstance()
    private val firestore = Firebase.firestore

    suspend fun loginProcess(user: String, password: String): LoginRegistrationResults {

        try {
            val result = firebaseAuth.signInWithEmailAndPassword(user, password).await()
            if (result.user != null) {
                return LoginRegistrationResults.LoginSuccess
            }
        } catch (e: FirebaseAuthException) {
            return LoginRegistrationResults.LoginError(e.localizedMessage)

        }

        return LoginRegistrationResults.LoginError("error")
    }

    suspend fun registrationProcess(user: String, password: String): LoginRegistrationResults {
        try {
            val result = firebaseAuth.createUserWithEmailAndPassword(user, password).await()
            val city = hashMapOf(
                "id" to result.user?.uid!!,
                "email" to result.user?.email
            )
            firestore.collection("users").document(result.user?.uid!!).set(city).await()
            if (result.user != null) {
                return LoginRegistrationResults.RegistrationSuccess
            }
        } catch (e: FirebaseAuthException) {
            return LoginRegistrationResults.RegistrationError(e.localizedMessage)
        }
        return LoginRegistrationResults.RegistrationError("common error")
    }

    suspend fun passwordRecoveryProcess(user: String?): LoginRegistrationResults {
        return try {
            val result = firebaseAuth.sendPasswordResetEmail(user ?: "").await()

            LoginRegistrationResults.RecoverSuccessful

        } catch (e: FirebaseAuthException) {
            LoginRegistrationResults.RecoverError(e.localizedMessage)
        }
        return LoginRegistrationResults.RecoverError("Common error")
    }

    suspend fun getUserReservations(currentUser: String): List<Tool> {
        var results: List<Tool> = emptyList()
        try {
            results = parseFirestoreQueryToTool(
                firestore.collection("users").document(currentUser).collection("reservations").get().await()
            )

        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            return results
        }
        return results
    }

    suspend fun deliverToolbyUser(user: String, idTool: String, email: String): Boolean {
        try {
            firestore.collection("users").document(user).collection("reservations").document(idTool)
                .delete().await()
            val reservation = HashMap<String, Any>()
            reservation["deliveryDate"] = Timestamp.now()
            reservation["available"] = true
            reservation["lastReservation"] = Timestamp.now()
            reservation["lastUser"] = email

            firestore.collection("tools").document(idTool).update(reservation).await()
            return true
        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            return false
        }
        return false
    }

    suspend fun reserveToolByUser(
        currentUser: String,
        tool: Tool,
        date: Date?,
        userId: String, endDate: Date?
    ): Boolean {
        try {
            val reservation = HashMap<String, Any>()
            reservation["deliveryDate"] = Timestamp(endDate!!)
            reservation["description"] = tool.description as String
            reservation["startDate"] = Timestamp(date!!)
            reservation["available"] = false
            reservation["lastMaintenance"] = tool.lastMaintenance as Timestamp
            reservation["lastReservation"] = Timestamp.now()
            reservation["lastUser"] = currentUser
            reservation["name"] = tool.name as String
            reservation["isCasilleroOpen"] = false
            reservation["casillero"] = tool.casillero as Long
            reservation["openChances"] = 2

            firestore.collection("users").document(userId).collection("reservations")
                .document(tool.id!!).set(reservation).await()
            Timber.d("Tools id is ${tool.id}")
            firestore.collection("tools").document(tool.id).update(reservation).await()
            return true
        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            return false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return false
    }

    suspend fun fetchToolData(): List<Tool> {
        var results: List<Tool> = emptyList()
        try {
            results = parseFirestoreQueryToTool(
                firestore.collection("tools").whereEqualTo("available", true).get().await()
            )
        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            return results
        }
        return results
    }

    suspend fun checkIfCanOpenLocker(user: String?, toolId: String?): Boolean {
        try {
            val result = parseFirestoreDocumentToTool(
                firestore.collection("users").document(
                    user ?: ""
                ).collection("reservations").document(toolId ?: "").get().await()
            )
            if (result.openchances != null)
                return result.openchances > 0

        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            return false
        }
        return false
    }

    suspend fun openLocker(user: String?, toolId: String?): Boolean {
        return try {
            val data = HashMap<String, Any>()
            data["isCasilleroOpen"] = true

            firestore.collection("users").document(
                user ?: ""
            ).collection("reservations").document(toolId ?: "")
                .update("openChances", FieldValue.increment(-1)).await()
            firestore.collection("users").document(
                user ?: ""
            ).collection("reservations").document(toolId ?: "")
                .update(data).await()
            true

        } catch (e: FirebaseFirestoreException) {
            e.printStackTrace()
            false
        }
    }


    private fun parseFirestoreDocumentToTool(documentSnapshot: DocumentSnapshot): Tool {
        return Tool(
            id = documentSnapshot.id,
            name = documentSnapshot.getString("name"),
            description = documentSnapshot.getString("description"),
            startDate = documentSnapshot.getTimestamp("startDate"),
            deliveryDate = documentSnapshot.getTimestamp("deliveryDate"),
            available = documentSnapshot.getBoolean("available"),
            lastUser = documentSnapshot.getString("lastUser"),
            lastReservation = documentSnapshot.getTimestamp("lastReservation"),
            lastMaintenance = documentSnapshot.getTimestamp("lastMaintenance"),
            casillero = documentSnapshot.getLong("casillero"),
            openchances = documentSnapshot.getLong("openChances")
        )
    }

    private fun parseFirestoreQueryToTool(query: QuerySnapshot): List<Tool> {
        val toolList = mutableListOf<Tool>()
        query.documents.mapTo(toolList) {
            Tool(
                id = it.id,
                name = it.getString("name"),
                description = it.getString("description"),
                startDate = it.getTimestamp("startDate"),
                deliveryDate = it.getTimestamp("deliveryDate"),
                available = it.getBoolean("available"),
                lastUser = it.getString("lastUser"),
                lastReservation = it.getTimestamp("lastReservation"),
                lastMaintenance = it.getTimestamp("lastMaintenance"),
                casillero = it.getLong("casillero"),
                openchances = it.getLong("openChances")
            )
        }
        return toolList
    }
}